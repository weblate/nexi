let dbName = 'NextI';

function getDbSchema() {
    let tblLists = {
        name: 'lists',
        columns: {
            id:   { primaryKey: true, autoIncrement: true               },
            name: { notNull: true,    dataType: 'string',  unique: true }
        }
    };
    let tblItems = {
        name: 'items',
        columns: {
            id:      { primaryKey: true, autoIncrement: true },
            name:    { notNull: true,    dataType: 'string'  },
            list_id: { notNull: true,    dataType: 'number'  },
            status:  { notNull: true,    dataType: 'string'  },
            number:  {                   dataType: 'number'  }
        }
    };
    let tblSettings = {
        name: 'settings',
        columns: {
            name: { primaryKey: true, dataType: 'string', unique: true },
            val:  { primaryKey: true, dataType: 'string'               }
        }
    };
    let db = {
        name: dbName,
        tables: [
            tblLists,
            tblItems,
            tblSettings
        ]
    };
    return db;
}

let connection = new JsStore.Connection(new Worker('js/jsstore.worker.min.js'));
async function initJsStore () {
    let   database    = getDbSchema();
    const isDbCreated = await connection.initDb(database);
    if (isDbCreated === true){
        console.log("db created");
        let lists = await connection.insert({
            into: 'lists',
            values: [ { name: 'Default list' } ],
            return: true
        });
        if (lists.length > 0) {
            console.log('Default list successfully added.');
            await setSetting('currentList', lists[0].id.toString());
        }
    }
    else {
        console.log("db opened");
    }
}

async function getLists () {
    return connection.select({
        from: 'lists',
        order: {
            by: 'name',
            type: 'asc'
        }
    });
}
async function getCurrent (list_id) {
    return connection.select({
        from: 'items',
        where: {
            status: 'current',
            list_id: list_id
        },
        order: {
            by: 'name',
            type: 'asc'
        }
    })
}
async function getNext (list_id) {
    return connection.select({
        from: 'items',
        where: {
            status: 'next',
            list_id: list_id
        },
        order: {
            by: 'name',
            type: 'asc'
        }
    })
}
async function getSetting (setting) {
    let current = await connection.select({
        from: 'settings',
        where: {
            name: setting
        }
    });
    if (current.length > 0) {
        return current[0].val;
    } else {
        return false;
    }
}
async function setSetting (setting, value) {
    let noOfRowsUpdated = await connection.insert({
        into: 'settings',
        values: [ { name: setting, val: value } ],
        upsert: true
    });
    if (noOfRowsUpdated === 0) {
        console.log('Unable to update current list record');
    }
    return noOfRowsUpdated;
}

async function initAll () {
    this.detectLang();
    document.getElementById('js-only').removeAttribute('id');
    document.getElementById('nl-name').value     = null;
    document.getElementById('ni-name').value     = null;
    document.getElementById('ni-number').value   = null;
    document.getElementById('ni-status').checked = false;
    this.$refs.importFile.value = null;
    await initJsStore();
    this.lists  = await getLists();
    let current = await getSetting('currentList');
    if (this.lists[0].id || current) {
        if (current) {
            this.current_list = parseInt(current);
        } else {
            this.current_list = this.lists[0].id;
        }
        this.current_list_name = this.currentList();
        this.current           = await getCurrent(this.current_list);
        this.next              = await getNext(this.current_list);
    }
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('js/nexi-sw.js');
    }
    let response = await fetch(`version.json?t=${(new Date).getTime()}`);
    if (response.ok) {
        let data = await response.json()
        if (typeof(data) !== 'undefined') {
            let version        = data.version;
            let currentVersion = parseInt(await getSetting('version')) || 0;
            await setSetting('version', `${version}`);
            if (currentVersion < version) {
                window.alert(this.i18n.need_reload);
                window.location.reload(true);
            }
        }
    }
}

function defaultData () {
    return {
        availableLangs: [
            { code: 'en', name: 'English'  },
            { code: 'fr', name: 'Français' }
        ],
        i18n: {
            add_item: 'Add item',
            add_new_list: 'Add a new list',
            cancel: 'Cancel',
            cant_remove_only_list: 'You can’t remove the only list you have.',
            confirm_list_removal: 'Are you sure you want to remove the current list and all its items?',
            current: 'Current',
            current_state: 'State: current?',
            decrement_number: 'Decrement number',
            default_list: 'Default list',
            edit_item: 'Change the name of the item',
            edit_list: 'Change the name of the list',
            export: 'Export your data',
            export_import: 'Export/import',
            import: 'Import data',
            import_ok: 'Your file has been successfully imported',
            increment_number: 'Increment number',
            list: 'List:',
            logo: 'Nexi’s logo',
            name: 'Name',
            name_taken: 'Name already taken, please choose another one',
            need_reload: 'New version available, the page will now reload.',
            new_list_name: 'Name of the new list',
            new_name: 'New name',
            next: 'Next',
            no_import_file: 'Please, select a file to import',
            number: 'Number',
            remove_item: 'Remove item',
            remove_list: 'Remove the current list and all its items',
            show_menu: 'Show menu',
        },
        current_lang: 'en',
        current_list: null,
        current_list_name: null,
        closed_menu: true,
        closed_lang_menu: true,
        closed_list_menu: true,
        closed_new_list_modal: true,
        closed_new_item_modal: true,
        closed_export_import_modal: true,
        closed_current: false,
        closed_next: false,
        lists: [],
        current: [],
        next: [],
        addListModal() {
            this.closed_new_list_modal = false;
            this.$nextTick(() => { document.getElementById('nl-name').focus(); })
        },
        addList() {
            let input = document.getElementById('nl-name');
            let name  = input.value;
            if (name) {
                connection.insert({
                    into: 'lists',
                    values: [ { name: name } ],
                    return: true
                }).then(async (newList) => {
                    if (newList.length > 0 && typeof(newList[0].id) !== 'undefined') {
                        console.log(`List ${name} successfully added.`);

                        input.value            = null;
                        this.current_list      = newList[0].id;
                        this.current_list_name = this.currentList();
                        this.current           = [];
                        this.next              = [];
                        let lists              = await getLists();
                        this.lists             = lists;
                        await setSetting('currentList', newList[0].id.toString());
                        this.closeModal();
                    } else {
                        console.error(`List ${name} fucked up.`);
                    }
                }).catch(spitJsStoreErr);
            } else {
                console.error(`The name of the new list is fucked up.`);
            }
        },
        editList() {
            let new_name = window.prompt(this.i18n.new_name, this.current_list_name);
            if (new_name !== null && new_name !== '' && new_name !== this.current_list_name) {
                connection.select({
                    from: 'lists',
                    where: {
                        name: new_name
                    }
                }).then(async (name_taken) => {
                    if (name_taken.length === 0) {
                        await connection.update({
                            in: 'lists',
                            set: {
                                name: new_name
                            },
                            where: {
                                id: this.current_list
                            }
                        });
                        this.current_list_name = new_name;
                    } else {
                        window.alert(this.i18n.name_taken);
                    }
                }).catch(spitJsStoreErr);
            }
        },
        removeList() {
            if (this.lists.length > 1) {
                let confirm = window.confirm(this.i18n.confirm_list_removal);
                if (confirm) {
                    connection.remove({
                        from: 'lists',
                        where: {
                            id: this.current_list
                        }
                    }).then(async (rowsDeleted) => {
                        if (rowsDeleted > 0) {
                            console.log(`Current list successfully deleted (id: ${this.current_list})`);
                            let rowsDeleted = await connection.remove({
                                from: 'items',
                                where: {
                                    list_id: this.current_list
                                }
                            });
                            let lists              = await getLists();
                            this.lists             = lists;
                            this.current_list      = lists[0].id;
                            this.current_list_name = this.currentList();
                            this.current           = await getCurrent(lists[0].id);
                            this.next              = await getNext(lists[0].id);
                            await setSetting('currentList', lists[0].id.toString());
                        } else {
                            console.error(`Error while trying to delete current list (id: ${this.current_list})`);
                        }
                    }).catch(spitJsStoreErr);
                }
            } else {
                alert(this.i18n.cant_remove_only_list);
                console.error(this.i18n.cant_remove_only_list);
            }
        },
        addItemModal() {
            this.closed_new_item_modal = false;
            this.$nextTick(() => { document.getElementById('ni-name').focus(); })
        },
        addItem() {
            let name   = document.getElementById('ni-name');
            let number = document.getElementById('ni-number');
            let status = document.getElementById('ni-status');
            let value = {
                name:    name.value,
                list_id: this.current_list,
                status:  (status.checked) ? 'current' : 'next'
            };
            if (number.value !== '') {
                value.number = parseInt(number.value);
            }
            connection.insert({
                into: 'items',
                values: [ value ],
                return: true
            }).then(async (item) => {
                if (status.checked) {
                    this.current = await getCurrent(this.current_list);
                } else {
                    this.next    = await getNext(this.current_list);
                }
                name.value     = null;
                number.value   = null;
                status.checked = false;
                this.closeModal();
            }).catch(spitJsStoreErr);
        },
        toggleItemStatus(event) {
            let node = event.target;
            while (node.tagName !== 'BUTTON') {
                node = node.parentNode;
            }
            let item_id = parseInt(node.getAttribute('data-id'));
            let status  = node.getAttribute('data-wannabe-status');
            connection.update({
                in: 'items',
                set: { status: status },
                where: { id: item_id }
            }).then(async (noOfRowsUpdated) => {
                if (noOfRowsUpdated > 0) {
                    this.current = await getCurrent(this.current_list);
                    this.next    = await getNext(this.current_list);
                } else {
                    console.error(`Couldn’t toggle item with id ${item_id}`);
                }
            }).catch(spitJsStoreErr);
        },
        incrementItem(event) {
            let node = event.target;
            while (node.tagName !== 'BUTTON') {
                node = node.parentNode;
            }
            let item_id = parseInt(node.getAttribute('data-id'));
            let number  = parseInt(node.getAttribute('data-nb')) + 1;
            let status  = node.getAttribute('data-status');
            connection.update({
                in: 'items',
                set: { number: number },
                where: { id: item_id }
            }).then(async (noOfRowsUpdated) => {
                if (noOfRowsUpdated > 0) {
                    console.log(`Item successfully upgraded (id: ${item_id})`);

                    if (status === 'current') {
                        this.current = await getCurrent(this.current_list);
                    } else {
                        this.next    = await getNext(this.current_list);
                    }
                } else {
                    console.error(`Couldn’t upgrade item with id ${item_id} to next number`);
                }
            }).catch(spitJsStoreErr);
        },
        decrementItem(event) {
            let node = event.target;
            while (node.tagName !== 'BUTTON') {
                node = node.parentNode;
            }
            let item_id = parseInt(node.getAttribute('data-id'));
            let number  = parseInt(node.getAttribute('data-nb'));
            if (number > 0) {
                number = number - 1;
                let status  = node.getAttribute('data-status');
                connection.update({
                    in: 'items',
                    set: { number: number },
                    where: { id: item_id }
                }).then(async (noOfRowsUpdated) => {
                    if (noOfRowsUpdated > 0) {
                        console.log(`Item successfully upgraded (id: ${item_id})`);

                        if (status === 'current') {
                            this.current = await getCurrent(this.current_list);
                        } else {
                            this.next    = await getNext(this.current_list);
                        }
                    } else {
                        console.error(`Couldn’t upgrade item with id ${item_id} to next number`);
                    }
                }).catch(spitJsStoreErr);
            }
        },
        editItem(event) {
            let node = event.target;
            while (node.tagName !== 'BUTTON') {
                node = node.parentNode;
            }
            let id      = parseInt(node.getAttribute('data-id'));
            let list_id = parseInt(node.getAttribute('data-list_id'));
            let name    = node.getAttribute('data-name');
            let status  = node.getAttribute('data-status');

            let new_name = window.prompt(this.i18n.new_name, name);
            if (new_name !== null && new_name !== '' && new_name !== name) {
                connection.select({
                    from: 'items',
                    where: {
                        name: new_name,
                        list_id: this.current_list
                    }
                }).then(async (name_taken) => {
                    console.log(name_taken);
                    console.log(name_taken.length);
                    if (name_taken.length === 0) {
                        await connection.update({
                            in: 'items',
                            set: {
                                name: new_name
                            },
                            where: {
                                id: id
                            }
                        });
                        if (status === 'current') {
                            this.current = await getCurrent(this.current_list);
                        } else {
                            this.next    = await getNext(this.current_list);
                        }
                    } else {
                        window.alert(this.i18n.name_taken);
                    }
                }).catch(spitJsStoreErr);
            }
        },
        removeItem(event) {
            let node = event.target;
            while (node.tagName !== 'BUTTON') {
                node = node.parentNode;
            }
            let item_id = parseInt(node.getAttribute('data-id'));
            let status  = node.getAttribute('data-status');
            connection.remove({
                from: 'items',
                where: {
                    id: item_id
                }
            }).then(async (rowsDeleted) => {
                if (rowsDeleted > 0) {
                    console.log(`Item successfully deleted (id: ${item_id})`);

                    if (status === 'current') {
                        this.current = await getCurrent(this.current_list);
                    } else {
                        this.next    = await getNext(this.current_list);
                    }
                } else {
                    console.error(`Error while trying to delete item (id: ${item_id})`);
                }
            }).catch(spitJsStoreErr);
        },
        changeSelectedList(event) {
            let list_id = parseInt(event.target.getAttribute('data-value'));
            setSetting('currentList', list_id.toString()).then(async (updated) => {
                if (updated > 0) {
                    this.current_list      = list_id;
                    this.current_list_name = this.currentList();
                    this.current           = await getCurrent(list_id);
                    this.next              = await getNext(list_id);
                    this.closed_list_menu  = true;
                }
            }).catch(spitJsStoreErr);
        },
        changeSelectedLang (event) {
            this.translate(event.target.getAttribute('data-value'));
            this.closed_lang_menu = true;
        },
        detectLang() {
            let languages = [];
            getSetting('lang').then((clang) => {
                if (clang) {
                    languages.push(clang);
                }
                let nl        = navigator.languages;
                for (let i = 0; i < nl.length; i++) {
                    languages.push(nl[i]);
                }
                if (languages.length !== 0) {
                    this.translate(languages.shift(), languages);
                } else {
                    console.log('Using old style language detection.');
                    let userLang = navigator.language || navigator.userLanguage;
                    this.translate(userLang);
                }
            }).catch((err) => {
                alert(err);
            });
        },
        translate(userLang, languages) {
            fetch('locales/'+userLang+'.json').then((response) => {
                if (!response.ok) {
                    if (typeof(languages) !== 'undefined' && languages !== null && languages.length !== 0) {
                        return this.translate(languages.shift(), languages);
                    }
                }
                return response.json();
            }).then(async (data) => {
                if (typeof(data) !== 'undefined') {
                    this.i18n = data;
                    this.current_lang = userLang;
                    await setSetting('lang', userLang);
                    this.$watch('lang', value => this.translate(value));
                }
            }).catch((err) => {
                console.log(`Could not download locales/${userLang}.json. Surely not translated in that language.`);
            });
        },
        currentLang () {
            let langs = this.availableLangs.filter((lang) => { return lang.code === this.current_lang });
            if (langs.length > 0 && typeof(langs[0].name) !== 'undefined') {
                return langs[0].name;
            }
            return '';
        },
        currentList() {
            if (this.lists.length < 1) {
                return '';
            } else {
                let currentLists = this.lists.filter((list) => { return parseInt(list.id) == this.current_list });
                if (currentLists.length > 0 && typeof(currentLists[0].name) !== 'undefined') {
                    return currentLists[0].name;
                }
                return '';
            }
        },
        closeModal() {
            if (!this.closed_new_list_modal || !this.closed_new_item_modal || !this.closed_export_import_modal) {
                this.closed_new_list_modal      = true;
                this.closed_new_item_modal      = true;
                this.closed_export_import_modal = true;
                document.getElementById('nl-name').value     = null;
                document.getElementById('ni-name').value     = null;
                document.getElementById('ni-number').value   = null;
                document.getElementById('ni-status').checked = false;
            }
        },
        exportImportModal() {
            this.closed_export_import_modal = false;
            this.$nextTick(() => { document.getElementById('export-data').focus(); })
        },
        exportData() {
            connection.select({ from: 'lists' }).then(async (lists) => {
                let items       = await connection.select({ from: 'items' });
                let storageData = [JSON.stringify({ lists: lists, items: items })];
                let exportFile  = new Blob(storageData, {type : 'application/json'});
                let url         = window.URL.createObjectURL(exportFile);

                this.$refs.exportDataJson.href = url;
                this.$refs.exportDataJson.download = 'export-data.json';
                this.$refs.exportDataJson.click();
            });
        },
        importData() {
            let f        = this.$refs.importFile.files;
            if (f.length === 0) {
                return window.alert(this.i18n.no_import_file);
            }
            let reader   = new FileReader();
            let tempThis = this;
            reader.addEventListener("loadend", async () => {
                try {
                    let data         = JSON.parse(reader.result);
                    let failed_lists = [];
                    let failed_items = [];
                    let mapped_id    = {};
                    for (let i = 0; i < data.lists.length; i++) {
                        let list   = data.lists[i];
                        let old_id = list.id;
                        delete list.id;
                        let results = await connection.select({
                            from: 'lists',
                            where: {
                                name: list.name
                            }
                        });
                        if (results.length > 0) {
                            mapped_id[old_id] = results[0].id;
                        } else {
                            let inserted = await connection.insert({
                                into: 'lists',
                                values: [ { name: list.name } ],
                                return: true
                            });
                            if (inserted.length > 0) {
                                mapped_id[old_id] = inserted[0].id;
                            } else {
                                failed_lists.push(list.name);
                            }
                        }
                    }
                    console.log(mapped_id);
                    for (let i = 0; i < data.items.length; i++) {
                        let item = data.items[i];
                        if (typeof(mapped_id[item.list_id]) === 'undefined') {
                            return;
                        }

                        delete item.id;
                        item.list_id = mapped_id[item.list_id];
                        let results = await connection.select({
                            from: 'items',
                            where: {
                                name: item.name,
                                list_id: item.list_id
                            }
                        });
                        while (results.length > 0) {
                            item.name = item.name + ' - imported';
                            results = await connection.select({
                                from: 'items',
                                where: {
                                    name: item.name,
                                    list_id: item.list_id
                                }
                            });
                        }
                        let inserted = await connection.insert({
                            into: 'items',
                            values: [ item ],
                            return: true
                        });
                        if (inserted.length === 0) {
                            failed_items.push(item.name);
                        }
                    }
                    tempThis.lists   = await getLists();
                    tempThis.current = await getCurrent(tempThis.current_list);
                    tempThis.next    = await getNext(tempThis.current_list);
                    if (failed_lists.length === 0 && failed_items.length === 0) {
                        alert(tempThis.i18n.import_ok);
                    } else {
                        let msg = tempThis.i18n.failed_imports + '\n';
                        if (failed_lists.length !== 0) {
                            msg = msg + tempThis.i18n.lists + '\n';
                            failed_lists.forEach((list) => {
                                msg = msg + '- ' + list + '\n';
                            });
                        }
                        if (failed_items.length !== 0) {
                            msg = msg + tempThis.i18n.items + '\n';
                            failed_items.forEach((item) => {
                                msg = msg + '- ' + item + '\n';
                            });
                        }
                        window.alert(msg);
                    }
                    tempThis.closeModal();
                } catch(err) {
                    alert(err);
                }
            });
            reader.readAsText(f[0]);
        }
    };
}

function spitJsStoreErr (err) {
    console.error(`${err.type}! ${err.message}`);
}
